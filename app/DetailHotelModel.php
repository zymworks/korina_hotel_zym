<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class DetailHotelModel extends Model
{
    protected $table = 'hotel_detail';
	public $timestamps = false;
	
}
