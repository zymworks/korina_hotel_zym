<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class RoomModel extends Model
{
    protected $table = 'master_room';
	public $timestamps = false;
	
}
