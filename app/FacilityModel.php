<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class FacilityModel extends Model
{
    protected $table = 'master_facility';
	public $timestamps = false;
	
}
