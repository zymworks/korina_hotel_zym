<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class CountryModel extends Model
{
    protected $table = 'master_country';
	public $timestamps = false;
	
}
