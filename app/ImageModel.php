<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class ImageModel extends Model
{
    protected $table = 'master_picture';
	public $timestamps = false;
	
}
