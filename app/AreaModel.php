<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model
{
    protected $table = 'master_area';
	public $timestamps = false;
	
}
