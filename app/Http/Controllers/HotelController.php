<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use DB;
use Image;
use App\HotelModel;
use App\DetailHotelModel;
use App\FacilityModel;
use App\RegionModel;
use App\CountryModel;
use App\AreaModel;
use App\CityModel;
use App\CurrencyModel;
use App\RoomModel;
use App\RoomBedModel;
use App\FoodModel;
use App\ImageModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#hotel\").DataTable({
						processing: true,
						serverSide: true,
						iDisplayLength: 25,
						ajax: \"".$url."/hotel/data\",
						columns: [
							{ data: 'id_from_mg'},
							{ data: 'id_to_agent'},
							{ data: 'hotel_name'},
							{ data: 'rating'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Hotel";
		return view('backend.hotel.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$hotel = HotelModel::select('*')->get();
		return Datatables::of($hotel)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($hotel) { return
				'<a href="'.url('/').'/backend/hotel/edit/'.$hotel->id.'/0/0" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/hotel/delete/'.$hotel->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

	public function detail_hotel($id){
		$crud = DetailHotelModel::where('id_hotel','=',$id)->first();
		$id_hotel = $crud->id_hotel;
		$js = '';
		$cruds = DB::table('hotel')
					->join('hotel_detail', 'hotel_detail.id_hotel', '=', 'hotel.id')
					->join('master_room', 'master_room.id_hotel', '=', 'hotel.id')
					->where('hotel_detail.id_hotel','=',$id)
					->where('master_room.id_hotel','=',$id)
					->select('hotel_detail.*','hotel.*','master_room.*')
					->distinct('')
					// ->limit(1)
					->get();
		$page['title']="Hotel Detail";
		// return redirect()->route('produk.edit',['id'=>$id_produk, 'idhal'=>'0', 'idedit'=>'0'])->with('alert-success', 'Data Berhasil Dihapus.');
		// return view('backend.item.detail')->with('js',$js)->with('css',$css)->with('page',$page);
		return view('backend.hotel.detail',compact('cruds'))->with('js',$js)->with('page',$page);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>";
		$page['title']="Hotel Add";
		$currency = CurrencyModel::pluck('currency_name','id');
		$currency->prepend('', '');
		return view('backend.hotel.formcreate', compact('currency'))->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
        $cruds = new HotelModel();
		$cruds->hotel_name = $request->hotel_name;
		$cruds->id_from_mg = $request->id_from_mg;
		$cruds->id_to_agent = $request->id_to_agent;
		$cruds->rating = $request->rating;
		$cruds->latitude = $request->latitude;
		$cruds->longitude = $request->longitude;
		$cruds->id_currency	= $request->id_currency;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			return redirect()->route('hotel.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('hotel.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $idhal, $idedit)
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script src=\''.$url.'/src/backend/plugins/tinymce/js/tinymce/tinymce.min.js\'></script>
			<script src=\''.$url.'/src/backend/assets/plugins/dropzone/dropzone.js\'></script>
			<script>
				$(\'.select2\').select2();

				function myFunctionDetail(id){
			
				$.ajaxSetup({
					headers: { \'X-CSRF-Token\' : $(\'meta[name=_token]\').attr(\'content\') }
					});	
					$.post(\''.$url.'/hotel/edit_detail_hotel\',
								{id: id},
									function(data2) {
									
											$(\'#hotel_room\').val(data2.hotel_room);
											$(\'#hotel_address\').val(data2.hotel_address);
											$(\'#hotel_location\').val(data2.hotel_location);
											$(\'#hotel_faximile\').val(data2.hotel_faximile);
											$(\'#hotel_email\').val(data2.hotel_email);
											$(\'#hotel_website\').val(data2.hotel_website);
											$(\'#id_hotel\').val(data2.id);
											$(\'#region_code\').val(data2.region_code).trigger("change");
											$(\'#country_code\').val(data2.country_code).trigger("change");
											$(\'#city_code\').val(data2.city_code).trigger("change");
											$(\'#add\').val(\'save\');
									},
									\'json\'
								);
				}

				tinymce.init({
				   path_absolute : \''.$url.'/\',
					selector: \'textarea.editdeskripsi\',
					plugins: [
					\'advlist autolink lists link image charmap print preview hr anchor pagebreak\',
					\'searchreplace wordcount visualblocks visualchars code fullscreen\',
					\'insertdatetime media nonbreaking save table contextmenu directionality\',
					\'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc\'
				  ],
				  toolbar1: \'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media\',
				  toolbar2: \'print preview media | forecolor backcolor emoticons | codesample\',
					relative_urls: false,
					file_browser_callback : function(field_name, url, type, win) {
					  var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(\'body\')[0].clientWidth;
					  var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName(\'body\')[0].clientHeight;

					  var cmsURL = \''.$url.'/\' + \'laravel-filemanager?field_name=\' + field_name;
					  if (type == \'image\') {
						cmsURL = cmsURL + \'&type=Images\';
					  } else {
						cmsURL = cmsURL + \'&type=Files\';
					  }

					  tinyMCE.activeEditor.windowManager.open({
						file : cmsURL,
						title : \'Filemanager\',
						width : x * 0.8,
						height : y * 0.8,
						resizable : \'yes\',
						close_previous : \'no\'
					  });
					}
				 });	
			</script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			<link href='".$url."/src/backend/assets/plugins/dropzone/dropzone.css' rel='stylesheet'>";
		$page['title']="Hotel Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		$cruds = HotelModel::find($id);
		$fasilitashotel = DB::table('hotel_facility')
					->join('hotel', 'hotel.id', '=', 'hotel_facility.id_hotel')
					->join('master_facility', 'master_facility.id', '=', 'hotel_facility.id_facility')
					->where('hotel.id', '=', $id)
					->select('hotel.*', 'master_facility.*', 'hotel_facility.*')
					->get();
		$detailhotel = DB::table('hotel_detail')
					->join('hotel', 'hotel.id', '=', 'hotel_detail.id_hotel')
					// ->join('master_region', 'master_region.id', '=', 'hotel_detail.region_code')
					->leftJoin('master_country', 'master_country.id', '=', 'hotel_detail.country_code')
					->leftJoin('master_city', 'master_city.id', '=', 'hotel_detail.city_code')
					// ->join('master_area', 'master_area.id', '=', 'hotel_detail.area_code')
					->where('hotel.id', '=', $id)
					->select('hotel.*', 'hotel_detail.*','master_country.*', 'master_city.*')
					// , 'master_region.*', 'master_country.*','master_city.*', 'master_area.*'
					->get();
		$roomhotel = DB::table('master_room')
					->join('hotel', 'hotel.id', '=', 'master_room.id_hotel')
					->leftJoin('master_food', 'master_food.id', '=', 'master_room.bf_type')
					->leftJoin('room_bed', 'room_bed.id', '=', 'master_room.id_bed_room')
					->where('hotel.id', '=', $id)
					->select('hotel.*', 'master_room.*','master_food.*', 'room_bed.*')
					// , 'master_region.*', 'master_country.*','master_city.*', 'master_area.*'
					->get();
		$gambar = DB::table('master_picture')
					->join('hotel', 'hotel.id', '=', 'master_picture.id_hotel')
					->join('master_room', 'master_room.id', '=', 'master_picture.id_room')
					->where('hotel.id', '=', $id)
					->select('hotel.*', 'master_picture.*', 'master_room.*')
					->get();
		$detail = DetailHotelModel::where('id_hotel', '=',$id)->get();
		$room = RoomModel::where('id_hotel', '=',$id)->get();
		$facility = FacilityModel::pluck('facility_name','id');
		$region = RegionModel::pluck('region_name','id');
		$country = CountryModel::pluck('country_name','id');
		$area = AreaModel::pluck('area_name','id');
		$city = CityModel::pluck('city_name','id');
		$currency = CurrencyModel::pluck('currency_name','id');
		$bedroom = RoomBedModel::pluck('room_type','id');
		$bf = FoodModel::pluck('type_name','id');
		$bf->prepend('', '');
		$bedroom->prepend('', '');
		$currency->prepend('', '');
		$facility->prepend('', '');
		$region->prepend('', '');
		$country->prepend('', '');
		$city->prepend('', '');
		$area->prepend('', '');
		if($idhal == '1'){
			session(['detail_tab' =>"2"]);
			$editdataitem=DetailHotelModel::where('id','=',$idedit)->first();
			return view('backend.hotel.formupdate', compact('gambar','bf','room','bedroom','detail','editdataitem','idedit','facility','region','country','area','city','currency','fasilitashotel','detailhotel','roomhotel'))->with('js',$js)->with('css',$css)->with('page',$page)->with('hotel', $cruds);
		} else if ($idhal == '2') {
			session(['detail_tab' =>"4"]);
			$editdataitem=RoomModel::where('id','=',$idedit)->first();
			return view('backend.hotel.formupdate', compact('gambar','bf','room','bedroom','detail','editdataitem','idedit','facility','region','country','area','city','currency','fasilitashotel','detailhotel','roomhotel'))->with('js',$js)->with('css',$css)->with('page',$page)->with('hotel', $cruds);
		} else {
			return view('backend.hotel.formupdate', compact('gambar','bf','room','bedroom','detail','facility','idedit','region','country','area','city','currency','fasilitashotel','detailhotel','roomhotel'))->with('js',$js)->with('css',$css)->with('page',$page)->with('hotel', $cruds);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	session(['detail_tab' =>"1"]);
		$session=session('login');
        $id=$request->id;
        $cruds = HotelModel::find($id);
		$cruds->hotel_name = $request->hotel_name;
		$cruds->id_from_mg = $request->id_from_mg;
		$cruds->id_to_agent = $request->id_to_agent;
		$cruds->rating = $request->rating;
		$cruds->latitude = $request->latitude;
		$cruds->longitude = $request->longitude;
		$cruds->id_currency	= $request->id_currency;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('hotel.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('hotel.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

	public function add_detail_hotel(Request $request){
		$session=session('login');
		session(['detail_tab' =>"2"]);

		$time = date('Y-m-d H:i:s');
		$idedit=$request->idedit;

		if($idedit != '0'){
			$cruds = DetailHotelModel::find($idedit);
		}else{
			$cruds = new DetailHotelModel();
		}

		$id = $request->id;
		$cruds->id_hotel = $request->id;
		$cruds->hotel_room = $request->hotel_room;
		$cruds->hotel_address = $request->hotel_address;;
		$cruds->region_code = $request->region_code;
		$cruds->country_code = $request->country_code;
		$cruds->city_code = $request->city_code;
		// $cruds->area_code = $request->area_code;
		$cruds->hotel_location = $request->hotel_location;
		$cruds->hotel_faximile = $request->hotel_faximile;
		$cruds->hotel_email = $request->hotel_email;
		$cruds->hotel_website = $request->hotel_website;
		$cruds->created_at = $time;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
        if($cruds->save()){
			return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('id', $id)->with('alert-success', 'Data Berhasil Disimpan.');
		}else{
			return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('id', $id)->with('alert-danger', 'Data Tidak Berhasil Disimpan.');;
		}
	}

	public function edit_detail_hotel(Request $request){
		$id = $request->id;
		$detail = DetailHotelModel::where('id','=',$id)->first();
		return $detail;
	 }

	 public function delete_detail_hotel($id,$id2){
		$cruds = DetailHotelModel::findOrFail($id2);
		if($cruds->delete()){
			   return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
	}

	public function add_room_hotel(Request $request){
		$session=session('login');
		session(['detail_tab' =>"4"]);

		$idedit=$request->idedit;
		if($idedit != '0'){
			$cruds = RoomModel::find($idedit);
		}else{
			$cruds = new RoomModel();
		}

		$id = $request->id;
		$cruds->id_hotel = $request->id;
		$cruds->room_description = $request->room_description;
		$cruds->room_name = $request->room_name;
		$cruds->bf_type = $request->bf_type;
		$cruds->child_min_age = $request->child_min_age;
		$cruds->child_max_age = $request->child_max_age;
		$cruds->room_min_stay = $request->room_min_stay;
		$cruds->net_price = $request->net_price;
		$cruds->GrossPrice = $request->GrossPrice;
		$cruds->CommPrice = $request->CommPrice;
		$cruds->id_bed_room = $request->id_bed_room;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
        if($cruds->save()){
			return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('id', $id)->with('alert-success', 'Data Berhasil Disimpan.');
		}else{
			return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('id', $id)->with('alert-danger', 'Data Tidak Berhasil Disimpan.');;
		}
	}

	public function edit_room_hotel(Request $request){
		$id = $request->id;
		$detail = RoomModel::where('id','=',$id)->first();
		return $detail;
	 }

	 public function delete_room_hotel($id,$id2){
		$cruds = RoomModel::findOrFail($id2);
		if($cruds->delete()){
			   return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('hotel.edit',['id'=>$id, 'idhal'=>'0', 'idedit'=>'0'])->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
	}

	public function upload_image(Request $request){
		$session=session('login');
		session(['detail_tab' =>"5"]);

		$image = $request->file('file');
		$judul = $request->file('slug');
		$tipe = "0";

		$filename = $image->getClientOriginalName();
		$namatok = pathinfo($filename, PATHINFO_FILENAME);
		$extension = $image->getClientOriginalExtension();

		$picture = $namatok.'_'.sha1($filename . time()) . '.' . $extension;

		// $destinationPaththumb = public_path('img/thumbnail');
		// $img = Image::make($image->getRealPath());
		// $img->resize(300, 300, function ($constraint) {
		//     $constraint->aspectRatio();
		// })->save($destinationPaththumb . '/' . $picture);

        $destinationPath = public_path() . 'img/hotel/'.$request->input('slug').'/';
		$image->move($destinationPath, $picture);
		// $hasil = Image::make($image)->save($destinationPath. '/' . $picture);

		$cruds = new ImageModel();
		$cruds->id_hotel = $request->input('id');
		$cruds->type_picture = $tipe;
		$cruds->name = $picture;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		// $picture->resize(640,360);
		$cruds->save(); 
        return response()->json(['success'=>$picture]);
	}

	public function upload_image_hotel(Request $request){
		$session=session('login');
		session(['detail_tab' =>"5"]);

		$tipe = "0";
		$image = $request->file('file');
		$filename = $image->getClientOriginalName();
		$judul = $request->input('slug');
		$namatok = pathinfo($filename, PATHINFO_FILENAME);
		$extension = $image->getClientOriginalExtension();
		$picture = $judul.'_'.sha1($filename . time()) . '.' . $extension;
        $destinationPath = public_path() . '/img/hotel/'.$request->input('slug').'/';
		$destinationPathold = public_path() . '/img/hotel/'.$request->input('slug').'/';
		$image->move($destinationPath, $picture);
		$cruds = new ImageModel();
		$cruds->id_hotel = $request->input('id');
		$cruds->name = $picture;
		$cruds->type_picture = $tipe;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		$cruds->save();
        return response()->json(['success'=>$picture]);
	}

	public function upload_image_room(Request $request){
		$session=session('login');
		session(['detail_tab' =>"6"]);

		$image = $request->file('file');
		$tipe = "1";

		$filename = $image->getClientOriginalName();
		$namatok = pathinfo($filename, PATHINFO_FILENAME);
		$extension = $image->getClientOriginalExtension();

		$picture = $namatok.'_'.sha1($filename . time()) . '.' . $extension;

		$destinationPaththumb = public_path() . 'img/thumb_room/' . $namatok . '/';
        $destinationPath = public_path() . 'img/room/' . $namatok . '/';
		$image->move($destinationPath, $picture);
		// $hasil = Image::make($image)->save($destinationPath. '/' . $picture);

		$cruds = new ImageModel();
		$cruds->id_hotel = $request->input('id');
		$cruds->type_picture = $tipe;
		$cruds->name = $picture;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		// $picture->resize(640,360);
		$cruds->save();  
        return response()->json(['success'=>$picture]);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = hotelModel::findOrFail($id);
		$cruds = HotelModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('hotel.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('hotel.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
