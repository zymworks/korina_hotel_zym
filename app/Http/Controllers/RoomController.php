<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\RoomModel;
use App\HotelModel;
use App\ImageModel;
use App\RoomBedModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#room\").DataTable({
						processing: true,
						serverSide: true,
						iDisplayLength: 50,
						ajax: \"".$url."/room/data\",
						columns: [
							{ data: 'hotel_name'},
							{ data: 'room_name'},
							{ data: 'room_description'},
							{ data: 'type_name'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Room";
		return view('backend.room.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		// $room = RoomModel::select('master_room.*', 'room_bed.*', 'master_food.*', 'hotel.*')
		// 		->join('hotel', 'hotel.id', '=', 'master_room.id_hotel')
		// 		->join('room_bed', 'master_room.id_bed_room', '=', 'room_bed.id')
		// 		->join('master_food', 'master_food.id', '=', 'master_room.bf_type')
		// 		->get();
				// print_r($room); exit();
		$room = RoomModel::select('master_room.*')
				->get();
		return Datatables::of($room)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($room) { return
				'<a href="'.url('/').'/backend/room/edit/'.$room->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/room/delete/'.$room->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/tinymce/js/tinymce/tinymce.min.js\'></script>
			<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();

				tinymce.init({
				   path_absolute : \''.$url.'/\',
					selector: \'textarea.adddeskripsi\',
					plugins: [
					\'advlist autolink lists link image charmap print preview hr anchor pagebreak\',
					\'searchreplace wordcount visualblocks visualchars code fullscreen\',
					\'insertdatetime media nonbreaking save table contextmenu directionality\',
					\'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc\'
				  ],
				  toolbar1: \'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media\',
				  toolbar2: \'print preview media | forecolor backcolor emoticons | codesample\',
					relative_urls: false,
					file_browser_callback : function(field_name, url, type, win) {
					  var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(\'body\')[0].clientWidth;
					  var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName(\'body\')[0].clientHeight;

					  var cmsURL = \''.$url.'/\' + \'laravel-filemanager?field_name=\' + field_name;
					  if (type == \'image\') {
						cmsURL = cmsURL + \'&type=Images\';
					  } else {
						cmsURL = cmsURL + \'&type=Files\';
					  }

					  tinyMCE.activeEditor.windowManager.open({
						file : cmsURL,
						title : \'Filemanager\',
						width : x * 0.8,
						height : y * 0.8,
						resizable : \'yes\',
						close_previous : \'no\'
					  });
					}
				 });
			</script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>";
		$page['title']="Room Add";
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', '');
		$bedroom = RoomBedModel::pluck('room_type','id');
		$bedroom->prepend('', '');
		return view('backend.room.formcreate', compact('hotel','bedroom'))->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
        $cruds = new RoomModel();
		$cruds->room_description = $request->room_description;
		$cruds->id_hotel = $request->id_hotel;
		$cruds->room_name = $request->room_name;
		$cruds->bf_type = $request->bf_type;
		$cruds->child_min_age = $request->child_min_age;
		$cruds->child_max_age = $request->child_max_age;
		$cruds->room_min_stay = $request->room_min_stay;
		$cruds->net_price = $request->net_price;
		$cruds->GrossPrice = $request->GrossPrice;
		$cruds->CommPrice = $request->CommPrice;
		$cruds->id_bed_room = $request->id_bed_room;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			return redirect()->route('room.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('room.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/tinymce/js/tinymce/tinymce.min.js\'></script>
			<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script src=\''.$url.'/src/backend/assets/plugins/dropzone/dropzone.js\'></script>
			<script>
				$(\'.select2\').select2();

				tinymce.init({
				   path_absolute : \''.$url.'/\',
					selector: \'textarea.editdeskripsi\',
					plugins: [
					\'advlist autolink lists link image charmap print preview hr anchor pagebreak\',
					\'searchreplace wordcount visualblocks visualchars code fullscreen\',
					\'insertdatetime media nonbreaking save table contextmenu directionality\',
					\'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc\'
				  ],
				  toolbar1: \'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media\',
				  toolbar2: \'print preview media | forecolor backcolor emoticons | codesample\',
					relative_urls: false,
					file_browser_callback : function(field_name, url, type, win) {
					  var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName(\'body\')[0].clientWidth;
					  var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName(\'body\')[0].clientHeight;

					  var cmsURL = \''.$url.'/\' + \'laravel-filemanager?field_name=\' + field_name;
					  if (type == \'image\') {
						cmsURL = cmsURL + \'&type=Images\';
					  } else {
						cmsURL = cmsURL + \'&type=Files\';
					  }

					  tinyMCE.activeEditor.windowManager.open({
						file : cmsURL,
						title : \'Filemanager\',
						width : x * 0.8,
						height : y * 0.8,
						resizable : \'yes\',
						close_previous : \'no\'
					  });
					}
				 });
			</script>
		';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			<link href='".$url."/src/backend/assets/plugins/dropzone/dropzone.css' rel='stylesheet'>";
		$page['title']="Room Edit";
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', '');
		$bedroom = RoomBedModel::pluck('room_type','id');
		$bedroom->prepend('', '');
		$cruds = RoomModel::find($id);
		return view('backend.room.formupdate', compact('hotel','bedroom'))->with('js',$js)->with('css',$css)->with('page',$page)->with('room', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$session=session('login');
        $id=$request->id;
        $cruds = RoomModel::find($id);
		$cruds->room_description = $request->room_description;
		$cruds->id_hotel = $request->id_hotel;
		$cruds->room_name = $request->room_name;
		$cruds->bf_type = $request->bf_type;
		$cruds->child_min_age = $request->child_min_age;
		$cruds->child_max_age = $request->child_max_age;
		$cruds->room_min_stay = $request->room_min_stay;
		$cruds->net_price = $request->net_price;
		$cruds->GrossPrice = $request->GrossPrice;
		$cruds->CommPrice = $request->CommPrice;
		$cruds->id_bed_room = $request->id_bed_room;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('room.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('room.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    public function upload_image_room(Request $request){
		$session=session('login');
		session(['detail_tab' =>"6"]);
		
		$image = $request->file('file');
		$tipe = "1";

		$filename = $image->getClientOriginalName();
		$namatok = pathinfo($filename, PATHINFO_FILENAME);
		$extension = $image->getClientOriginalExtension();

		$picture = $namatok.'_'.sha1($filename . time()) . '.' . $extension;

		$destinationPaththumb = public_path() . 'img/thumb_room/' . $namatok . '/';
        $destinationPath = public_path() . 'img/room/' . $namatok . '/';
		$image->move($destinationPath, $picture);
		// $hasil = Image::make($image)->save($destinationPath. '/' . $picture);

		$cruds = new ImageModel();
		$cruds->id_hotel = $request->input('id');
		$cruds->type_picture = $tipe;
		$cruds->name = $picture;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		// $picture->resize(640,360);
		$cruds->save(); 
        return response()->json(['success'=>$picture]);
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = roomModel::findOrFail($id);
		$cruds = RoomModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('room.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('room.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
