<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\RegionModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>			
			<script>
			$(function() {
					var table = $(\"#region\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/region/data\",
						columns: [
							{ data: 'region_name'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">";
		$page['title']="Region";
		return view('backend.region.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$region = RegionModel::select('*')->get();
		return Datatables::of($region)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($region) { return
				'<a href="'.url('/').'/backend/region/edit/'.$region->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/region/delete/'.$region->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js=" ";
		$css=" ";

		$page['title']="Region Add";
		return view('backend.region.formcreate')->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
        $cruds = new RegionModel();
		$cruds->region_name = $request->region_name;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			return redirect()->route('region.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('region.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js=" ";
		$css=" ";
		$page['title']="Region Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		$cruds = RegionModel::find($id);
		return view('backend.region.formupdate')->with('js',$js)->with('css',$css)->with('page',$page)->with('region', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$session=session('login');
        $id=$request->id;
        $cruds = RegionModel::find($id);
		$cruds->region_name = $request->region_name;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('region.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('region.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = regionModel::findOrFail($id);
		$cruds = RegionModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('region.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('region.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
