<?php

namespace App\Http\Controllers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Session;
use App\UserModel;
use App\Http\Requests;


class HomeController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
	function __construct() {
		// $this->middleware('SystemRuleGuide');

	}
	
    public function index(Request $request) {
		$url=url('/');
		$js="<script src=\"".$url."/src/backend/plugins/jquery/dist/jquery.min.js\"></script>";
		$css="<link href=\"".$url."/src/backend/css/bootstrap.min.css\" rel=\"stylesheet\">";
		$page['title']="Dashboard | Korina Hotel Admin";
		return view('backend.index')->with('js',$js)->with('css',$css)->with('page',$page);
    }

    public function login()
    {
		$sessionlogin=session('login');
		if(!isset($sessionlogin)){
			return view('backend.login');
		}else{
			return redirect()->route('home');
		}
    }

    public function login_submit(Request $request){
			$cruds = new UserModel();
			$username=$request->username;
			$password=md5(md5($request->password));
			$log_in = DB::table('system_user')
            ->select('system_user.*')
			->where('username','=',$username)
			->where('password','=',$password)
			->first();
			if(!empty($log_in)){
				$data=array(		
					'username' =>$log_in->username,
					'user_group_id' =>$log_in->user_group_id,
					'user_name' =>$log_in->user_name
				);
				session(['login'=>$data]);
				return redirect()->route('home');
			}else{
				$gagal='<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<strong>Invalid username / password</strong>
				</div>';
				session(['gagal'=>$gagal]);
				 return redirect()->route('korinaadmin');
			}
	}

}