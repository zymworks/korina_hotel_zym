<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\CityModel;
use App\CountryModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#city\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/city/data\",
						columns: [
							{ data: 'city_name'},
							{ data: 'country_name'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="City";
		return view('backend.city.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$city = CityModel::select('*')
				->join('master_country', 'master_city.id_country', '=', 'master_country.id')
				->get();
		return Datatables::of($city)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($city) { return
				'<a href="'.url('/').'/backend/city/edit/'.$city->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/city/delete/'.$city->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>";
		$page['title']="City Add";
		$country = CountryModel::pluck('country_name','id');
		$country->prepend('', '');
		return view('backend.city.formcreate', compact('country'))->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$session=session('login');
        $cruds = new CityModel();
		$cruds->city_name = $request->city_name;
		$cruds->id_country = $request->id_country;
		$cruds->created_at = date('Y-m-d H:i:s');
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			return redirect()->route('city.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('city.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>";
		$page['title']="City Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		$cruds = CityModel::find($id);
		$country = CountryModel::pluck('country_name','id');
		$country->prepend('', '');
		return view('backend.city.formupdate', compact('country'))->with('js',$js)->with('css',$css)->with('page',$page)->with('city', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$session=session('login');
        $id=$request->id;
        $cruds = CityModel::find($id);
		$cruds->city_name = $request->city_name;
		$cruds->id_country = $request->id_country;
		$cruds->created_by = $session['username'];
		$cruds->last_update = date('Y-m-d H:i:s');
		if($cruds->save()){
			  return redirect()->route('city.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('city.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = cityModel::findOrFail($id);
		$cruds = CityModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('city.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('city.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
