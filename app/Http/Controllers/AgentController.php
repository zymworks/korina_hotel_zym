<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\AgentModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#agent\").DataTable({
						processing: true,
						serverSide: true,
						idSrc: \"id_agent\",
						ajax: \"".$url."/agent/data\",
						columns: [
							{ data: 'agent_name'},
							{ data: 'username'},
							{ data: 'password'},
							{ data: 'last_balance'},
							{ data: 'action', 'searchable': false, 'orderable':false }
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Agent";
		return view('backend.agent.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$agent = AgentModel::select(['*'])->get();
		return Datatables::of($agent)
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($agent) { return
				'<a href="'.url('/').'/backend/agent/edit/'.$agent->id_agent.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/backend/agent/delete/'.$agent->id_agent.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'Anda yakin akan menghapus data ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url=url('/');
		$js=" ";
		$css=" ";

		$page['title']="Agent Add";
		return view('backend.agent.formcreate')->with('js',$js)->with('css',$css)->with('page',$page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	// $this->validate($request, [
	    //     'password'              => 'required|min:6',
	    //     'password_confirmation' => 'required|confirmed'
   		// ]);
        $cruds = new AgentModel();
		$cruds->agent_name = $request->agent_name;
		$cruds->username = $request->username;
		$cruds->password = md5($request->password);
		$cruds->last_balance = $request->last_balance;
		if($cruds->save()){
			return redirect()->route('agent.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('agent.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $url=url('/');
		$js=" ";
		$css=" ";
		$page['title']="Agent Edit";
		// $option=self::$option;
		// $benua = ContinentModel::pluck('continent_name_en','id');
		$cruds = AgentModel::find($id);
		return view('backend.agent.formupdate')->with('js',$js)->with('css',$css)->with('page',$page)->with('agent', $cruds);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    	// $this->validate($request, [
	    //     'old_password'          => 'required',
	    //     'password'              => 'required|min:6',
	    //     'password_confirmation' => 'required|confirmed'
   		// ]);
		
        $id=$request->id;
        $cruds = AgentModel::find($id);
		$cruds->agent_name = $request->agent_name;
		$cruds->username = $request->username;
		$cruds->password = md5($request->password);
		$cruds->last_balance = $request->last_balance;
		if($cruds->save()){
			  return redirect()->route('agent.edit',['id'=>$id])->with('alert-success', 'Data Berhasil Diubah.');
		 }
		 else{
			 return redirect()->route('agent.edit',['id'=>$id])->with('alert-danger', 'Data Tidak Berhasil Diubah.');
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cruds = areaModel::findOrFail($id);
		$cruds = AgentModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('agent.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('agent.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }
}
