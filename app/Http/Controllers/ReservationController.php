<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Datatables;
use App\ReservationModel;
use App\ReservationDetailModel;
use App\HotelModel;
use App\Http\Requests;
use Illuminate\Support\Str;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	function __construct()
	{
		$this->middleware('SystemRule');
	}
	
	
    public function index()
    {
        $url=url('/');
		$js="<script src=\"".$url."/src/backend/assets/plugins/jquery-quicksearch/jquery.quicksearch.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.buttons.min.js\"></script>
			<script src=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedHeader.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.keyTable.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.responsive.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/responsive.bootstrap.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.scroller.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.js\"></script>	
			<script src=\"".$url."/src/backend/assets/plugins/datatables/dataTables.fixedColumns.min.js\"></script>	
			<script src=\"".$url."/src/backend/assets/pages/datatables.init.js\"></script>					
			<script>
			$(function() {
					var table = $(\"#listorder\").DataTable({
						processing: true,
						serverSide: true,
						ajax: \"".$url."/listorder/data\",
						columns: [
							{ data: 'name'},
							{ data: 'hotel_name'},
							{ data: 'price'},
							{ data: 'adult'},
							{ data: 'child'},
							{ data: 'room_total'},
							{ data: 'action', 'searchable': false, 'orderable':false },
							{ data: 'status'}
						],
					});
				});
				
		</script>";
		$css="<link href=\"".$url."/src/backend/assets/plugins/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/buttons.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedHeader.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/scroller.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.colVis.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/dataTables.bootstrap.min.css\" rel=\"stylesheet\">
			<link href=\"".$url."/src/backend/assets/plugins/datatables/fixedColumns.dataTables.min.css\" rel=\"stylesheet\">";
		$page['title']="Reservation List";
		return view('backend.reservation.list')->with('js',$js)->with('css',$css)->with('page',$page);
    }
	
	public function data_show(Request $request){
	   if($request->ajax()){
		$url=url('/');
		$listorder = ReservationDetailModel::select('sales.*','hotel.*','sales_detail.*')
					->join('sales', 'sales.id', '=', 'sales_detail.id_sales')
					->join('hotel', 'hotel.id', '=', 'sales_detail.id_hotel')
					->orderBy('sales_detail.id', 'asc')
					->get();
		return Datatables::of($listorder)
			
				// tambah kolom untuk aksi edit dan hapus
				->addColumn('action',function ($listorder) { return
				'<a href="'.url('/').'/listorder/edit/'.$listorder->id.'" title="Edit" class="btn-sm btn-warning"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
				<a href="'.url('/').'/listorder/delete/'.$listorder->id.'" title="Delete" class="btn-sm btn-danger" onclick="return confirm(\'You sure will delete  ?\');"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				';})
				->addColumn('status',function ($listorder) { return
					$listorder->status == 1 ? '<label class="label label-info"> prosessing </label>' : ($listorder->status == 2 ? '<label class="label label-success">booked</label>' : ($listorder->status == 3 ? '<label class="label label-warning">cancel</label>' : '<label class="label label-danger">wrong')) ;
				})
				->make(true);
		} else {
			exit("Not an AJAX request -_-");
		}
	}
	
	public function create(){
		$url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>
			<script src=\''.$url.'/src/backend/plugins/datepicker/js/bootstrap-datepicker.js\'></script>
			<script type="text/javascript">
            $(document).ready(function () {
                $(\'.date\').datepicker({
                    format: \'yyyy-mm-dd\',
                    autoclose:true
                });
            });
        </script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			  <link href='".$url."/src/backend/plugins/datepicker/css/bootstrap-datepicker3.css' rel='stylesheet'>";
		$page['title']="Reservation Add";
		$sales = ReservationModel::pluck('name','id');
		$sales->prepend('', '');
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', ''); 
		return view('backend.reservation.formcreate', compact('sales','hotel'))->with('js',$js)->with('css',$css)->with('page',$page);
	}

	public function store(Request $request){
		$cruds = new ReservationDetailModel();
		$cruds->id_sales	= $request->id_sales;
		$cruds->id_hotel	= $request->id_hotel;
		$cruds->id_room		= $request->id_room;
		$cruds->check_in 	= $request->check_in;
		$cruds->check_out 	= $request->check_out;
		$cruds->price 		= $request->price;
		$cruds->adult 		= $request->adult;
		$cruds->child 		= $request->child;
		$cruds->room_total 	= $request->room_total;
		if($cruds->save()){
			return redirect()->route('reservation.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('reservation.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
	}

	public function edit($id){
		$url=url('/');
		$js='<script src=\''.$url.'/src/backend/plugins/select2/select2.full.min.js\'></script>
			<script>
				$(\'.select2\').select2();
			</script>
			<script src=\''.$url.'/src/backend/plugins/datepicker/js/bootstrap-datepicker.js\'></script>
			<script type="text/javascript">
            $(document).ready(function () {
                $(\'.date\').datepicker({
                    format: \'yyyy-mm-dd\',
                    autoclose:true
                });
            });
        </script>';
		$css="<link href='".$url."/src/backend/plugins/select2/select2.min.css' rel='stylesheet'>
			  <link href='".$url."/src/backend/plugins/datepicker/css/bootstrap-datepicker3.css' rel='stylesheet'>";
		$page['title']="Reservation Edit";

		$cruds = ReservationDetailModel::find($id);

		$sales = ReservationModel::pluck('name','id');
		$sales->prepend('', '');
		$hotel = HotelModel::pluck('hotel_name','id');
		$hotel->prepend('', ''); 
		return view('backend.reservation.formupdate', compact('sales','hotel'))->with('js',$js)->with('css',$css)->with('page',$page)->with('reservation', $cruds);
	}

	public function update(Request $request){
		$id=$request->id;
        $cruds = ReservationDetailModel::find($id);
		$cruds->id_sales	= $request->id_sales;
		$cruds->id_hotel	= $request->id_hotel;
		$cruds->id_room		= $request->id_room;
		$cruds->check_in 	= $request->check_in;
		$cruds->check_out 	= $request->check_out;
		$cruds->price 		= $request->price;
		$cruds->adult 		= $request->adult;
		$cruds->child 		= $request->child;
		$cruds->room_total 	= $request->room_total;
		if($cruds->save()){
			return redirect()->route('reservation.create')->with('alert-success', 'Data Berhasil Disimpan.');
		}
		else{
			return redirect()->route('reservation.create')->with('alert-danger', 'Data Tidak Berhasil Disimpan.');
		}
	}

	public function destroy($id)
    {
        // $cruds = cityModel::findOrFail($id);
		$cruds = ReservationDetailModel::find($id);
		if($cruds->delete()){
			  return redirect()->route('reservation.index')->with('alert-success', 'Data Berhasil Dihapus.');
		 }
		 else{
			 return redirect()->route('reservation.index')->with('alert-danger', 'Data Tidak Berhasil Dihapus.');
		 }
    }

}

