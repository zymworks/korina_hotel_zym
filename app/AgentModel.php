<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class AgentModel extends Model
{
    protected $table = 'agent';
    protected $primaryKey = 'id_agent';
	public $timestamps = false;
	
}
